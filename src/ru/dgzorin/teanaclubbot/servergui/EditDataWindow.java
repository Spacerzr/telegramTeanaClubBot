package ru.dgzorin.teanaclubbot.servergui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import ru.dgzorin.teanaclubbot.database.Database;

public class EditDataWindow extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -600940195731442158L;
	private static final int WINDOW_WIDTH = 300;
	private static final int WINDOW_HEIGHT = 300;
	private final String sucsessChangeData = "Данные в таблице успешно изменены !";
	private final String errorChangeData = "Необходимо заполнить все поля !";

	private final JPanel mainPanel = new JPanel(new BorderLayout());
	private final JPanel secondPanel = new JPanel(new FlowLayout());
	private final JLabel label = new JLabel("Выберите таблицу для редактирования данных:");
	private final Color lightBlue = new Color(120, 175, 255);

	private final JButton btnTableMembers = new JButton("Таблица \"Участники\"");
	private final JButton btnTableLinks = new JButton("Таблица \"Полезные ссылки\"");
	private final JButton btnTableServices = new JButton("Таблица \"Сервисы\"");

	private final JButton btnTableFaqGeneral = new JButton("Таблица \"Общие вопросы\"");
	private final JButton btnTableFaqj31 = new JButton("Таблица \"FAQ J31\"");
	private final JButton btnTableFaqj32 = new JButton("Таблица \"FAQ J32\"");
	private final JButton btnTableFaql33 = new JButton("Таблица \"FAQ L33\"");
	private final JButton btnTableFaqLiquids = new JButton("Таблица \"Жидкости\"");

	private JFrame editData;
	private JFrame editLinksData;
	private JFrame editServicesData;
	private JFrame editMembers;

	private JFrame windowApplyChange;

	EditDataWindow() {
		initGUI();
	}

	private void initGUI() {

		setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		setLocationRelativeTo(null);
		setTitle("Редактирование");
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		secondPanel.setBackground(Color.GRAY);
		label.setOpaque(true);
		label.setBackground(lightBlue);

		btnTableMembers.setBackground(lightBlue);
		btnTableLinks.setBackground(lightBlue);
		btnTableServices.setBackground(lightBlue);

		btnTableFaqGeneral.setBackground(lightBlue);
		btnTableFaqj31.setBackground(lightBlue);
		btnTableFaqj32.setBackground(lightBlue);
		btnTableFaql33.setBackground(lightBlue);
		btnTableFaqLiquids.setBackground(lightBlue);

		secondPanel.add(btnTableMembers);
		secondPanel.add(btnTableLinks);
		secondPanel.add(btnTableServices);

		secondPanel.add(btnTableFaqGeneral);
		secondPanel.add(btnTableFaqj31);
		secondPanel.add(btnTableFaqj32);
		secondPanel.add(btnTableFaql33);
		secondPanel.add(btnTableFaqLiquids);

		mainPanel.add(label, BorderLayout.NORTH);
		mainPanel.add(secondPanel, BorderLayout.CENTER);
		add(mainPanel);

		btnTableFaqGeneral.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				editData("faq_general");
			}
		});

		btnTableFaqj31.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				editData("faq_j31");
			}
		});

		btnTableFaqj32.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				editData("faq_j32");
			}
		});

		btnTableFaql33.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				editData("faq_l33");
			}
		});

		btnTableFaqLiquids.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				editData("faq_liquids");
			}
		});

		btnTableLinks.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				editLinks();
			}
		});

		btnTableServices.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				editServices();
			}
		});

		btnTableMembers.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				editMembers();
			}
		});
		addWindowListener(new WindowListener() {

			@Override
			public void windowClosing(WindowEvent e) {
				Database.disconnect();
				ServerTeanaClub.sendMsgToLog("Редактирование данных завершено.");
				ServerTeanaClub.sendMsgToLog("Disconnected from data base.");
			}

			@Override
			public void windowOpened(WindowEvent e) {
				try {
					Database.connect();
					ServerTeanaClub.sendMsgToLog("Connect to database was successful");
				} catch (Exception ex) {
					ServerTeanaClub.sendMsgToLog("Connect to database : ERROR");
				}
			}

			@Override
			public void windowIconified(WindowEvent e) {
			}

			@Override
			public void windowDeiconified(WindowEvent e) {
			}

			@Override
			public void windowDeactivated(WindowEvent e) {
			}

			@Override
			public void windowClosed(WindowEvent e) {
			}

			@Override
			public void windowActivated(WindowEvent e) {
			}
		});
	}

	private void editData(String tableName) {

		editData = new JFrame("Таблица " + tableName);
		int width = 500;
		int height = 500;
		editData.setSize(width, height);
		editData.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		editData.setLocationRelativeTo(null);
		editData.setResizable(false);

		JPanel mainPanel = new JPanel(new GridLayout(5, 1));

		JPanel panel1 = new JPanel(new FlowLayout());
		JPanel panel2 = new JPanel(new FlowLayout());
		JPanel panel3 = new JPanel(new FlowLayout());
		JPanel panel4 = new JPanel(new FlowLayout());
		JPanel panel5 = new JPanel(new FlowLayout());
		JPanel panel6 = new JPanel(new FlowLayout());

		JComboBox<String> comboBox = new JComboBox<String>();
		JLabel label1 = new JLabel(" Выберите запись из списка : ");

		JLabel label2 = new JLabel("      Описание : ");
		JLabel label3 = new JLabel("  Имя файла фото описания : ");
		JLabel label4 = new JLabel("    Имя файла фото решения : ");
		JLabel label5 = new JLabel("    Решение : ");

		JTextArea taDescription = new JTextArea(5, 28);
		JTextField tfPhotoDescription = new JTextField(20);
		JTextField tfPhotoDecision = new JTextField(20);
		JTextArea taDecision = new JTextArea(5, 28);
		JScrollPane scrollPane = new JScrollPane(taDescription, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		JScrollPane scrollPane2 = new JScrollPane(taDecision, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		JButton btnEdit = new JButton("Изменить");
		JButton btnCreate = new JButton("Добавить");
		JButton btnRemove = new JButton("Удалить");

		btnEdit.setEnabled(false);
		btnCreate.setEnabled(false);
		btnRemove.setEnabled(false);

		ResultSet rs = Database.getData(tableName);

		HashMap<String, ResultSet> map = new HashMap<>();
		try {
			comboBox.addItem("добавить новую запись");
			while (rs.next()) {
				comboBox.addItem(rs.getString(3));
				map.put(rs.getString(3), Database.getOneStringData(tableName, rs.getString(3)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		comboBox.setSelectedItem(null);
		taDescription.setLineWrap(true);
		taDescription.setWrapStyleWord(true);
		taDecision.setLineWrap(true);
		taDecision.setWrapStyleWord(true);

		label1.setOpaque(true);
		label1.setBackground(lightBlue);
		label2.setOpaque(true);
		label2.setBackground(lightBlue);
		label3.setOpaque(true);
		label3.setBackground(lightBlue);
		label4.setOpaque(true);
		label4.setBackground(lightBlue);
		label5.setOpaque(true);
		label5.setBackground(lightBlue);

		panel1.setBackground(Color.GRAY);
		panel2.setBackground(Color.GRAY);
		panel3.setBackground(Color.GRAY);
		panel4.setBackground(Color.GRAY);
		panel5.setBackground(Color.GRAY);
		panel6.setBackground(Color.GRAY);

		panel1.add(label1);
		panel1.add(comboBox);
		panel2.add(label2);
		panel2.add(scrollPane);
		panel3.add(label3);
		panel3.add(tfPhotoDescription);
		panel3.add(label4);
		panel3.add(tfPhotoDecision);
		panel4.add(label5);
		panel4.add(scrollPane2);
		panel5.add(btnCreate);
		panel5.add(btnEdit);
		panel5.add(btnRemove);

		mainPanel.add(panel1);
		mainPanel.add(panel2);
		mainPanel.add(panel3);
		mainPanel.add(panel4);
		mainPanel.add(panel5);

		editData.add(mainPanel);
		editData.setVisible(true);

		comboBox.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {

				if (!comboBox.getSelectedItem().equals("добавить новую запись")) {
					btnEdit.setEnabled(true);
					btnRemove.setEnabled(true);
					btnCreate.setEnabled(false);

					try {
						taDescription.setText(comboBox.getSelectedItem().toString());

						tfPhotoDescription.setText(map.get(comboBox.getSelectedItem().toString()).getString(3));
						tfPhotoDecision.setText(map.get(comboBox.getSelectedItem()).getString(5));
						taDecision.setText(map.get(comboBox.getSelectedItem()).getString(4));
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				} else {
					btnCreate.setEnabled(true);
					btnEdit.setEnabled(false);
					btnRemove.setEnabled(false);
					tfPhotoDecision.setText("");
					tfPhotoDescription.setText("");
					taDecision.setText("");
					taDescription.setText("");
				}

			}
		});

		btnEdit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (comboBox.getSelectedItem() != null) {
					try {
						Integer id = map.get(comboBox.getSelectedItem()).getInt(6);
						Database.editDataFaq(tableName, id, taDescription.getText(), tfPhotoDescription.getText(),
								taDecision.getText(), tfPhotoDecision.getText());
					} catch (SQLException e) {
						e.printStackTrace();
					} catch (NumberFormatException e) {
						ServerTeanaClub.sendMsgToLog("Ошибка ! Неверные данные!");
					}

					editData.dispose();
					windowApplyChange(tableName, sucsessChangeData);
					ServerTeanaClub.sendMsgToLog(
							"Изменены данные в таблице " + tableName + " запись :  " + taDescription.getText() + ".");
				}
			}
		});

		btnCreate.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Integer id = 1;
				String temp = taDescription.getText();

				for (HashMap.Entry<String, ResultSet> pair : map.entrySet()) {
					id++;
				}

				if (temp.equals("") || tfPhotoDecision.getText().equals("") || tfPhotoDescription.getText().equals("")
						|| taDescription.getText().equals("") || taDecision.getText().equals("")) {
					windowApplyChange(tableName, errorChangeData);
					return;
				}

				try {
					Database.addNewToFaq(tableName, id, taDescription.getText(), tfPhotoDescription.getText(),
							taDecision.getText(), tfPhotoDecision.getText());
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				editData.dispose();
				windowApplyChange(tableName, sucsessChangeData);
				ServerTeanaClub.sendMsgToLog("Добавлено новая запись" + temp + " в таблицу " + tableName + ".");
			}
		});

		btnRemove.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Database.removeFromData(tableName, taDescription.getText());
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				editData.dispose();
				windowApplyChange(tableName, sucsessChangeData);
				ServerTeanaClub
						.sendMsgToLog("Удалена запись" + taDescription.getText() + " из таблицы " + tableName + ".");
			}
		});
	}

	private void editLinks() {
		editLinksData = new JFrame("Таблица Полезные ссылки");
		int width = 500;
		int height = 500;
		editLinksData.setSize(width, height);
		editLinksData.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		editLinksData.setLocationRelativeTo(null);
		editLinksData.setResizable(false);

		JPanel mainPanel = new JPanel(new GridLayout(5, 1));

		JPanel panel1 = new JPanel(new FlowLayout());
		JPanel panel2 = new JPanel(new FlowLayout());
		JPanel panel3 = new JPanel(new FlowLayout());
		JPanel panel4 = new JPanel(new FlowLayout());
		JPanel panel5 = new JPanel(new FlowLayout());
		JPanel panel6 = new JPanel(new FlowLayout());

		JComboBox<String> comboBox = new JComboBox<String>();
		JLabel label1 = new JLabel(" Выберите запись из списка : ");
		JLabel label2 = new JLabel("      Описание : ");
		JLabel label3 = new JLabel("  Имя файла фото описания : ");
		JLabel label4 = new JLabel("    Url страницы : ");

		JTextArea taDescription = new JTextArea(5, 28);
		JTextField tfPhotoDescription = new JTextField(20);
		JScrollPane scrollPane = new JScrollPane(taDescription, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		JTextArea taUrl = new JTextArea(5, 28);
		JScrollPane scrollPane2 = new JScrollPane(taUrl, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		JButton btnEdit = new JButton("Изменить");
		JButton btnCreate = new JButton("Добавить");
		JButton btnRemove = new JButton("Удалить");

		btnEdit.setEnabled(false);
		btnCreate.setEnabled(false);
		btnRemove.setEnabled(false);

		ResultSet rs = Database.getLinksData();

		HashMap<String, ResultSet> map = new HashMap<>();
		try {
			comboBox.addItem("добавить новую запись");
			while (rs.next()) {
				comboBox.addItem(rs.getString(3));
				map.put(rs.getString(3), Database.getOneStringLinksData(rs.getString(3)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		comboBox.setSelectedItem(null);
		taDescription.setLineWrap(true);
		taDescription.setWrapStyleWord(true);
		taUrl.setLineWrap(true);
		taUrl.setWrapStyleWord(true);

		label1.setOpaque(true);
		label1.setBackground(lightBlue);
		label2.setOpaque(true);
		label2.setBackground(lightBlue);
		label3.setOpaque(true);
		label3.setBackground(lightBlue);
		label4.setOpaque(true);
		label4.setBackground(lightBlue);

		panel1.setBackground(Color.GRAY);
		panel2.setBackground(Color.GRAY);
		panel3.setBackground(Color.GRAY);
		panel4.setBackground(Color.GRAY);
		panel5.setBackground(Color.GRAY);
		panel6.setBackground(Color.GRAY);

		panel1.add(label1);
		panel1.add(comboBox);
		panel2.add(label2);
		panel2.add(scrollPane);
		panel3.add(label3);
		panel3.add(tfPhotoDescription);
		panel4.add(label4);
		panel4.add(scrollPane2);
		panel5.add(btnCreate);
		panel5.add(btnEdit);
		panel5.add(btnRemove);

		mainPanel.add(panel1);
		mainPanel.add(panel2);
		mainPanel.add(panel3);
		mainPanel.add(panel4);
		mainPanel.add(panel5);

		editLinksData.add(mainPanel);
		editLinksData.setVisible(true);

		comboBox.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {

				if (!comboBox.getSelectedItem().equals("добавить новую запись")) {
					btnEdit.setEnabled(true);
					btnRemove.setEnabled(true);
					btnCreate.setEnabled(false);

					try {
						taDescription.setText(comboBox.getSelectedItem().toString());

						tfPhotoDescription.setText(map.get(comboBox.getSelectedItem().toString()).getString(3));
						taUrl.setText(map.get(comboBox.getSelectedItem()).getString(4));
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				} else {
					btnCreate.setEnabled(true);
					btnEdit.setEnabled(false);
					btnRemove.setEnabled(false);
					tfPhotoDescription.setText("");
					taUrl.setText("");
					taDescription.setText("");
				}
			}
		});

		btnEdit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (comboBox.getSelectedItem() != null) {
					try {
						Integer id = map.get(comboBox.getSelectedItem()).getInt(5);
						Database.editDataLinks(id, taDescription.getText(), tfPhotoDescription.getText(),
								taUrl.getText());
					} catch (SQLException e) {
						e.printStackTrace();
					} catch (NumberFormatException e) {
						ServerTeanaClub.sendMsgToLog("Ошибка ! Неверные данные!");
					}

					editLinksData.dispose();
					windowApplyChange("links", sucsessChangeData);
					ServerTeanaClub.sendMsgToLog(
							"Изменены данные в таблице Полезные ссылки, запись :  " + taDescription.getText() + ".");
				}
			}
		});

		btnCreate.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Integer id = 1;
				String temp = taDescription.getText();

				for (HashMap.Entry<String, ResultSet> pair : map.entrySet()) {
					id++;
				}

				if (temp.equals("") || tfPhotoDescription.getText().equals("") || taUrl.getText().equals("")) {
					windowApplyChange("links", errorChangeData);
					return;
				}

				try {
					Database.addNewToLinks(id, taDescription.getText(), tfPhotoDescription.getText(), taUrl.getText());
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				editLinksData.dispose();
				windowApplyChange("links", sucsessChangeData);
				ServerTeanaClub.sendMsgToLog("Добавлено новая запись" + temp + " в таблицу Полезные сслыки.");
			}
		});

		btnRemove.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Database.removeFromData("links", taDescription.getText());
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				editLinksData.dispose();
				windowApplyChange("links", sucsessChangeData);
				ServerTeanaClub.sendMsgToLog(
						"Удалена запись " + taDescription.getText() + " из таблицы " + "Полезные ссылки" + ".");
			}
		});
	}

	private void editServices() {
		editServicesData = new JFrame("Таблица Сервисы");
		int width = 500;
		int height = 500;
		editServicesData.setSize(width, height);
		editServicesData.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		editServicesData.setLocationRelativeTo(null);
		editServicesData.setResizable(false);

		JPanel mainPanel = new JPanel(new GridLayout(5, 1));

		JPanel panel1 = new JPanel(new FlowLayout());
		JPanel panel2 = new JPanel(new FlowLayout());
		JPanel panel3 = new JPanel(new FlowLayout());
		JPanel panel4 = new JPanel(new FlowLayout());
		JPanel panel5 = new JPanel(new FlowLayout());
		JPanel panel6 = new JPanel(new FlowLayout());

		JComboBox<String> comboBox = new JComboBox<String>();
		JLabel label1 = new JLabel(" Выберите запись из списка : ");
		JLabel label2 = new JLabel("    Название : ");
		JLabel label3 = new JLabel("      Описание : ");
		JLabel label4 = new JLabel("  Имя файла фото описания : ");

		JTextArea taDescription = new JTextArea(5, 28);
		JTextField tfPhotoDescription = new JTextField(20);
		JTextArea taName = new JTextArea(5, 28);
		;

		JScrollPane scrollPane = new JScrollPane(taDescription, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		JScrollPane scrollPane2 = new JScrollPane(taName, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		JButton btnEdit = new JButton("Изменить");
		JButton btnCreate = new JButton("Добавить");
		JButton btnRemove = new JButton("Удалить");

		btnEdit.setEnabled(false);
		btnCreate.setEnabled(false);
		btnRemove.setEnabled(false);

		ResultSet rs = Database.getServicesData();

		HashMap<String, ResultSet> map = new HashMap<>();
		try {
			comboBox.addItem("добавить новую запись");
			while (rs.next()) {
				comboBox.addItem(rs.getString(2));
				map.put(rs.getString(2), Database.getOneStringServicesData(rs.getString(2)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		comboBox.setSelectedItem(null);
		taDescription.setLineWrap(true);
		taDescription.setWrapStyleWord(true);
		taName.setLineWrap(true);
		taName.setWrapStyleWord(true);

		label1.setOpaque(true);
		label1.setBackground(lightBlue);
		label2.setOpaque(true);
		label2.setBackground(lightBlue);
		label3.setOpaque(true);
		label3.setBackground(lightBlue);
		label4.setOpaque(true);
		label4.setBackground(lightBlue);

		panel1.setBackground(Color.GRAY);
		panel2.setBackground(Color.GRAY);
		panel3.setBackground(Color.GRAY);
		panel4.setBackground(Color.GRAY);
		panel5.setBackground(Color.GRAY);
		panel6.setBackground(Color.GRAY);

		panel1.add(label1);
		panel1.add(comboBox);
		panel2.add(label2);
		panel2.add(scrollPane2);
		panel3.add(label3);
		panel3.add(scrollPane);
		panel4.add(label4);
		panel4.add(tfPhotoDescription);
		panel5.add(btnCreate);
		panel5.add(btnEdit);
		panel5.add(btnRemove);

		mainPanel.add(panel1);
		mainPanel.add(panel2);
		mainPanel.add(panel3);
		mainPanel.add(panel4);
		mainPanel.add(panel5);

		editServicesData.add(mainPanel);
		editServicesData.setVisible(true);

		comboBox.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {

				if (!comboBox.getSelectedItem().equals("добавить новую запись")) {
					btnEdit.setEnabled(true);
					btnRemove.setEnabled(true);
					btnCreate.setEnabled(false);

					try {
						taDescription.setText(map.get(comboBox.getSelectedItem().toString()).getString(2));

						tfPhotoDescription.setText(map.get(comboBox.getSelectedItem().toString()).getString(3));
						taName.setText(comboBox.getSelectedItem().toString());
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				} else {
					btnCreate.setEnabled(true);
					btnEdit.setEnabled(false);
					btnRemove.setEnabled(false);
					tfPhotoDescription.setText("");
					taName.setText("");
					taDescription.setText("");
				}
			}
		});

		btnEdit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (comboBox.getSelectedItem() != null) {
					try {
						Integer id = map.get(comboBox.getSelectedItem()).getInt(4);
						Database.editDataServices(id, taName.getText(), taDescription.getText(),
								tfPhotoDescription.getText());
					} catch (SQLException e) {
						e.printStackTrace();
					} catch (NumberFormatException e) {
						ServerTeanaClub.sendMsgToLog("Ошибка ! Неверные данные!");
					}

					editServicesData.dispose();
					windowApplyChange("services", sucsessChangeData);
					ServerTeanaClub.sendMsgToLog(
							"Изменены данные в таблице Сервисы, запись : " + taDescription.getText() + ".");
				}
			}
		});

		btnCreate.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Integer id = 1;
				String temp = taDescription.getText();

				for (HashMap.Entry<String, ResultSet> pair : map.entrySet()) {
					id++;
				}

				if (temp.equals("") || tfPhotoDescription.getText().equals("") || taDescription.getText().equals("")) {
					windowApplyChange("services", errorChangeData);
					return;
				}

				try {
					Database.addNewToServices(id, taName.getText(), taDescription.getText(),
							tfPhotoDescription.getText());
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				editServicesData.dispose();
				windowApplyChange("services", sucsessChangeData);
				ServerTeanaClub.sendMsgToLog("Добавлено новая запись" + temp + " в таблицу Сервисы.");
			}
		});

		btnRemove.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Database.removeFromServices(comboBox.getSelectedItem().toString());
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				editServicesData.dispose();
				windowApplyChange("services", sucsessChangeData);
				ServerTeanaClub.sendMsgToLog("Удалена запись " + taDescription.getText() + " из таблицы " + "Сервисы.");
			}
		});
	}

	private void editMembers() {
		editMembers = new JFrame("Таблица Сервисы");
		int width = 400;
		int height = 250;
		editMembers.setSize(width, height);
		editMembers.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		editMembers.setLocationRelativeTo(null);
		editMembers.setResizable(false);

		JPanel mainPanel = new JPanel(new GridLayout(4, 1));

		JPanel panel1 = new JPanel(new FlowLayout());
		JPanel panel2 = new JPanel(new FlowLayout());
		JPanel panel3 = new JPanel(new FlowLayout());
		JPanel panel4 = new JPanel(new FlowLayout());

		JComboBox<String> comboBox = new JComboBox<String>();
		JComboBox<String> comboBox2 = new JComboBox<String>();

		JLabel label1 = new JLabel(" Выберите пользователя из списка : ");
		JLabel label2 = new JLabel("    Номер телефона : ");
		JLabel label3 = new JLabel("      Масс. рассылка : ");

		JTextField tfPhoneNumber = new JTextField(20);

		JButton btnSave = new JButton("Сохранить");

		HashMap<String, ResultSet> map = new HashMap<>();
		ResultSet rs = Database.getMembers();
		try {
			while (rs.next()) {
				comboBox.addItem(rs.getString(2));
				map.put(rs.getString(2), Database.getOneMember(rs.getString(2)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		comboBox.setSelectedItem(null);
		comboBox2.addItem("on");
		comboBox2.addItem("off");

		try {
			comboBox2.setSelectedItem(rs.getString(5));
		} catch (SQLException e) {
			comboBox2.setSelectedItem(null);
		}

		label1.setOpaque(true);
		label1.setBackground(lightBlue);
		label2.setOpaque(true);
		label2.setBackground(lightBlue);
		label3.setOpaque(true);
		label3.setBackground(lightBlue);

		panel1.setBackground(Color.GRAY);
		panel2.setBackground(Color.GRAY);
		panel3.setBackground(Color.GRAY);
		panel4.setBackground(Color.GRAY);

		panel1.add(label1);
		panel1.add(comboBox);
		panel2.add(label2);
		panel2.add(tfPhoneNumber);
		panel3.add(label3);
		panel3.add(comboBox2);
		panel4.add(btnSave);

		mainPanel.add(panel1);
		mainPanel.add(panel2);
		mainPanel.add(panel3);
		mainPanel.add(panel4);

		editMembers.add(mainPanel);
		editMembers.setVisible(true);

		comboBox.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {

				try {
					tfPhoneNumber.setText(map.get(comboBox.getSelectedItem().toString()).getString(4));
					try {
						comboBox2.setSelectedItem(map.get(comboBox.getSelectedItem().toString()).getString(5));
					} catch (SQLException ex) {
						comboBox2.setSelectedItem(null);
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});

		btnSave.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Database.editMember(comboBox.getSelectedItem().toString(), tfPhoneNumber.getText(),
							comboBox2.getSelectedItem().toString());
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				editMembers.dispose();
				windowApplyChange("members", sucsessChangeData);
				ServerTeanaClub.sendMsgToLog("Изменены данные в таблице Участники для пользователя "
						+ comboBox.getSelectedItem().toString() + ".");

			}
		});

	}

	private void windowApplyChange(String tableName, String text) {
		windowApplyChange = new JFrame("");
		JPanel mainPanel = new JPanel(new FlowLayout());
		JLabel label = new JLabel(text);
		JButton btnOk = new JButton("OK");

		int width = 250;
		int height = 90;
		windowApplyChange.setSize(width, height);
		windowApplyChange.setResizable(false);
		windowApplyChange.setLocationRelativeTo(null);

		btnOk.setPreferredSize(new Dimension(160, 30));
		label.setOpaque(true);
		label.setBackground(lightBlue);
		mainPanel.setBackground(Color.GRAY);
		mainPanel.add(label);
		mainPanel.add(btnOk);

		windowApplyChange.add(mainPanel);
		windowApplyChange.setVisible(true);

		btnOk.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				switch (tableName) {
				case "faq_j31":
				case "faq_j32":
				case "faq_l33":
				case "faq_liquids":
				case "faq_general":
					if (text.equals(sucsessChangeData)) {
						editData(tableName);
					}
					break;
				case "links":
					if (text.equals(sucsessChangeData)) {
						editLinks();
					}
				case "services":
					if (text.equals(sucsessChangeData)) {
						editServices();
					}
				case "members":
					if (text.equals(sucsessChangeData)) {
						editMembers();
					}
				default:
					break;
				}
				windowApplyChange.dispose();
			}
		});
	}
	//
	// private void sendChangeToLog(String tableName) {
	// ServerTeanaClub.sendMsgToLog("Изменены данные в таблице " + tableName);
	// }

}
