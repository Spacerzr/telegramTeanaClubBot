package ru.dgzorin.teanaclubbot.servergui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import ru.dgzorin.teanaclubbot.database.Database;
import ru.dgzorin.teanaclubbot.telegrambot.TelegramBot;

public class ServerTeanaClub {

	private static final JTextArea log = new JTextArea(15, 30);
	private static SimpleDateFormat date = new SimpleDateFormat("dd.MM.yy 'at' HH.mm ':'");
	private JFrame viewServer;
	private JFrame viewspamSettings;
	private JFrame viewSpamForOneUseSettings;
	private JFrame windowError;

	private EditDataWindow viewEditData = new EditDataWindow();

	private Color lightGreen = new Color(180, 255, 180);
	private Color lightRed = new Color(255, 155, 155);
	private Color lightBlue = new Color(120, 175, 255);

	public static void main(String[] args) {
		SwingUtilities.invokeLater(ServerTeanaClub::new);
	}

	ServerTeanaClub() {
		initGUI();
	}

	private void initGUI() {

		viewServer = new JFrame();
		JPanel mainPanel = new JPanel(new FlowLayout());
		JPanel downPanel = new JPanel(new GridLayout(3, 2));
		JLabel telBotText = new JLabel("Telegram Bot");
		JLabel tmp = new JLabel("");
		JLabel tmp2 = new JLabel("");

		int MAIN_WINDOW_WIDTH = 400;
		int MAIN_WINDOW_HEIGHT = 370;

		// JTextArea log = new JTextArea(15, 30);
		JScrollPane scrollPane = new JScrollPane(log, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		JButton btnStartTelBot = new JButton("Запустить");
		JButton btnStopTelBot = new JButton("Остановить");
		JButton btnSpamSettings = new JButton("Массовая рассылка");
		JButton btnSpamToOneUserSettings = new JButton("Сообщение 1 клиенту");

		JButton btnEditDB = new JButton("Редактировать");

		JLabel temp = new JLabel("");

		viewServer.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		viewServer.setSize(MAIN_WINDOW_WIDTH, MAIN_WINDOW_HEIGHT);
		viewServer.setLocationRelativeTo(null);
		viewServer.setTitle("Nissan Teana Club Server");
		viewServer.setResizable(false);
		log.setEditable(false);
		log.setLineWrap(true);
		log.setWrapStyleWord(true);

		// btnStopTelBot.setEnabled(false);
		btnStartTelBot.setEnabled(true);
		btnSpamSettings.setEnabled(false);
		btnSpamToOneUserSettings.setEnabled(false);
		mainPanel.setBackground(Color.GRAY);
		telBotText.setOpaque(true);
		telBotText.setBackground(lightRed);
		tmp.setOpaque(true);
		tmp.setBackground(Color.GRAY);
		tmp2.setOpaque(true);
		tmp2.setBackground(Color.GRAY);

		btnStartTelBot.setBackground(lightGreen);
		// btnSettings.setBackground(lightBlue);
		btnSpamSettings.setBackground(lightBlue);
		btnSpamToOneUserSettings.setBackground(lightBlue);

		btnEditDB.setBackground(lightBlue);
		temp.setOpaque(true);

		temp.setBackground(Color.GRAY);
		downPanel.add(telBotText);
		downPanel.add(btnStartTelBot);
		// downPanel.add(btnStopTelBot);
		// downPanel.add(btnSettings);
		downPanel.add(btnEditDB);
		downPanel.add(tmp);

		downPanel.add(btnSpamSettings);
		downPanel.add(btnSpamToOneUserSettings);
		// downPanel.add(tmp);
		// downPanel.add(tmp2);

		mainPanel.add(scrollPane);
		mainPanel.add(downPanel);

		viewServer.add(mainPanel);

		viewServer.setVisible(true);

		btnStartTelBot.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				sendMsgToLog("Nissan Teana Club server !");
				try {
					Database.connect();
					sendMsgToLog("Connect to database was successful");
				} catch (Exception ex) {
					sendMsgToLog("Connect to database : ERROR");
				}

				try {
					TelegramBot.startTelegramBot();
					sendMsgToLog("Telegram Bot was started !\n");
					btnStartTelBot.setEnabled(false);
					btnEditDB.setEnabled(false);
					btnSpamSettings.setEnabled(true);
					btnSpamToOneUserSettings.setEnabled(true);
					// btnSpamSettings.setEnabled(false);
					// btnStopTelBot.setBackground(lightRed);
					// btnStopTelBot.setEnabled(true);
					telBotText.setBackground(lightGreen);
				} catch (Exception ex) {
					sendMsgToLog("Start Telegram Bot Fail !\n");
				}

			}
		});

		btnEditDB.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				viewEditData.setVisible(true);
				sendMsgToLog("Редактирование данных ...");
			}
		});

		btnSpamSettings.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				initSpamSettingsWindow();
			}
		});

		btnSpamToOneUserSettings.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				initSpamForOneUseSettingsWindow();
			}
		});
	}

	private void initSpamSettingsWindow() {
		viewspamSettings = new JFrame();
		JPanel mainPanel = new JPanel(new GridLayout(3, 1));
		JPanel upPanel = new JPanel(new FlowLayout());
		JPanel centerPanel = new JPanel(new FlowLayout());
		JPanel downPanel = new JPanel(new FlowLayout());

		JLabel labelPhoto = new JLabel("Введите имя файла. Файл должен находиться в папке Photos.");
		JLabel labelText = new JLabel("Введите текст : ");
		JLabel labelInstr1 = new JLabel("Заполните поля и сохраните изменения.");
		JLabel labelInstr2 = new JLabel("Для отправки рассылки отправьте боту команду /sendSpam");

		JTextField namePhoto = new JTextField(20);
		JTextArea description = new JTextArea(4, 29);
		JScrollPane scrollPane = new JScrollPane(description, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		JButton btnSaveSettings = new JButton("Сохранить");

		int width = 400;
		int height = 300;

		viewspamSettings.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		viewspamSettings.setSize(width, height);
		viewspamSettings.setLocationRelativeTo(null);
		viewspamSettings.setTitle("Настройки массовой рассылки");
		viewspamSettings.setResizable(false);

		mainPanel.setBackground(Color.GRAY);
		upPanel.setBackground(Color.GRAY);
		centerPanel.setBackground(Color.GRAY);
		downPanel.setBackground(Color.GRAY);
		btnSaveSettings.setBackground(lightGreen);
		labelPhoto.setOpaque(true);
		labelPhoto.setBackground(lightBlue);
		labelText.setOpaque(true);
		labelText.setBackground(lightBlue);
		labelInstr1.setOpaque(true);
		labelInstr1.setBackground(lightBlue);
		labelInstr2.setOpaque(true);
		labelInstr2.setBackground(lightBlue);

		description.setLineWrap(true);
		description.setWrapStyleWord(true);
		namePhoto.setText(TelegramBot.spamPhotoName);
		description.setText(TelegramBot.spamDescription);

		upPanel.add(labelPhoto);
		upPanel.add(namePhoto);
		centerPanel.add(labelText);
		centerPanel.add(scrollPane);
		downPanel.add(labelInstr1);
		downPanel.add(labelInstr2);

		downPanel.add(btnSaveSettings);

		mainPanel.add(upPanel);
		mainPanel.add(centerPanel);
		mainPanel.add(downPanel);

		viewspamSettings.add(mainPanel);
		viewspamSettings.setVisible(true);

		btnSaveSettings.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int len = description.getText().length();
				if (len > 200) {
					windowError(len);
				} else {
					TelegramBot.setSpamSettings(namePhoto.getText(), description.getText());
					sendMsgToLog("Настройки массовой рассылки сохранены. Файл : " + namePhoto.getText()
							+ ", Описание : " + description.getText() + ".\n");
					viewspamSettings.dispose();
				}
			}
		});
	}

	private void initSpamForOneUseSettingsWindow() {
		viewSpamForOneUseSettings = new JFrame();
		JPanel mainPanel = new JPanel(new GridLayout(3, 1));
		JPanel upPanel = new JPanel(new FlowLayout());
		JPanel centerPanel = new JPanel(new FlowLayout());
		JPanel downPanel = new JPanel(new FlowLayout());

		JLabel labelChatId = new JLabel(" Выберите пользователя, которому будет отправлено сообщение.");
		JLabel labelText = new JLabel(" Введите текст : ");
		JLabel labelInstr1 = new JLabel(" Заполните поля и сохраните изменения.");
		JLabel labelInstr2 = new JLabel(" Для отправки сообщения одному клиенту ");
		JLabel labelInstr3 = new JLabel("      отправьте боту команду /sendMsgToOneUser   \n ");

		JComboBox<String> comboBox = new JComboBox<String>();

		JTextField nameChatId = new JTextField(20);
		JTextArea description = new JTextArea(4, 29);
		JScrollPane scrollPane = new JScrollPane(description, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		JButton btnSaveSettings = new JButton("Сохранить");

		int width = 400;
		int height = 330;

		viewSpamForOneUseSettings.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		viewSpamForOneUseSettings.setSize(width, height);
		viewSpamForOneUseSettings.setLocationRelativeTo(null);
		viewSpamForOneUseSettings.setTitle("Настройки сообщения одному клиенту");
		viewSpamForOneUseSettings.setResizable(false);

		ResultSet rs = Database.getMembers();
		try {
			while (rs.next()) {
				comboBox.addItem(rs.getString(2));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		mainPanel.setBackground(Color.GRAY);
		upPanel.setBackground(Color.GRAY);
		centerPanel.setBackground(Color.GRAY);
		downPanel.setBackground(Color.GRAY);
		btnSaveSettings.setBackground(lightGreen);
		labelChatId.setOpaque(true);
		labelChatId.setBackground(lightBlue);
		labelText.setOpaque(true);
		labelText.setBackground(lightBlue);
		labelInstr1.setOpaque(true);
		labelInstr1.setBackground(lightBlue);
		labelInstr2.setOpaque(true);
		labelInstr2.setBackground(lightBlue);
		labelInstr3.setOpaque(true);
		labelInstr3.setBackground(lightBlue);

		description.setLineWrap(true);
		description.setWrapStyleWord(true);
		nameChatId.setText(TelegramBot.chatIdForMsgToOneUser);
		description.setText(TelegramBot.spamForOneUser);

		upPanel.add(labelChatId);
		upPanel.add(comboBox);
		centerPanel.add(labelText);
		centerPanel.add(scrollPane);
		downPanel.add(labelInstr1);
		downPanel.add(labelInstr2);
		downPanel.add(labelInstr3);

		downPanel.add(btnSaveSettings);

		mainPanel.add(upPanel);
		mainPanel.add(centerPanel);
		mainPanel.add(downPanel);

		viewSpamForOneUseSettings.add(mainPanel);
		viewSpamForOneUseSettings.setVisible(true);

		btnSaveSettings.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int len = description.getText().length();
				if (len > 180) {
					windowError(len);
				} else {
					TelegramBot.setSpamForOneUserSettings(Database.getOneChatId(comboBox.getSelectedItem().toString()),
							description.getText());
					sendMsgToLog("Настройки отправки сообщения одному клиенту сохранены. Сообщение : "
							+ description.getText() + ".\n");
					viewSpamForOneUseSettings.dispose();
				}
			}
		});
	}

	private void windowError(int len) {
		windowError = new JFrame("Error");
		JPanel mainPanel = new JPanel(new FlowLayout());
		JLabel label = new JLabel("Слишком длинное описание !");
		JLabel label2 = new JLabel("Количество символов не должно ");
		JLabel label3 = new JLabel("превышать 180.У вас " + len + ".");

		JButton btnOk = new JButton("OK");
		// text.setLineWrap(true);
		// text.setEditable(false);
		int width = 250;
		int height = 130;
		windowError.setSize(width, height);
		windowError.setResizable(false);
		windowError.setLocationRelativeTo(null);

		btnOk.setPreferredSize(new Dimension(160, 30));
		label.setOpaque(true);
		label.setBackground(lightBlue);
		label2.setOpaque(true);
		label2.setBackground(lightBlue);
		label3.setOpaque(true);
		label3.setBackground(lightBlue);
		mainPanel.setBackground(Color.GRAY);
		mainPanel.add(label);
		mainPanel.add(label2);
		mainPanel.add(label3);
		mainPanel.add(btnOk);

		windowError.add(mainPanel);
		windowError.setVisible(true);

		btnOk.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				windowError.dispose();
			}
		});
	}

	public static void sendMsgToLog(String msg) {
		log.append(date.format(new Date()) + " " + msg + "\n");
	}
}
