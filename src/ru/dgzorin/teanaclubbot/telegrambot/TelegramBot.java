package ru.dgzorin.teanaclubbot.telegrambot;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.send.SendPhoto;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import ru.dgzorin.teanaclubbot.database.Database;

public class TelegramBot extends TelegramLongPollingBot {

	private static HashMap<String, String> members = new HashMap<String, String>();

	private SendMessage sendMessage = new SendMessage();
	private Keyboards chooseKeyboard = new Keyboards();
	private ArrayList<KeyboardRow> keyboard = new ArrayList<>();

	private static HashMap<String, String> spamMsgs = new HashMap<String, String>();
	private static HashMap<String, Boolean> spamMsgsFlags = new HashMap<String, Boolean>();

	private static HashMap<String, String> PhoneNumbers = new HashMap<String, String>();
	private static HashMap<String, Boolean> PhoneNumbersFlags = new HashMap<String, Boolean>();

	private static String adminChatId = "308710480";
	public static String spamPhotoName = "ntc_logo";
	public static String spamDescription = "Пожелания по добавлению информации в разделы Вы можете присылать мне:"
			+ " https://vk.com/zorindm." + "\nС уважением, разработчик Nissan Teana Club Bot Зорин Дмитрий.";

	public static String spamForOneUser = "Пожелания по добавлению информации в разделы Вы можете присылать мне:"
			+ " https://vk.com/zorindm.\nС уважением, разработчик Nissan Teana Club Bot Зорин Дмитрий.";
	public static String chatIdForMsgToOneUser = "308710480";

	public static void startTelegramBot() {

		ApiContextInitializer.init();
		TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
		try {
			Database.connect();
			telegramBotsApi.registerBot(new TelegramBot());
		} catch (TelegramApiException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public String getBotUsername() {
		return "teana_club_bot";
	}

	@Override
	public String getBotToken() {
		return "566483575:AAEqw-fpwURRnCI25ZAjI67bKXwF7b03VqQ";
	}

	@Override
	public void onUpdateReceived(Update update) {

		Message message = update.getMessage();
		Boolean spamMsgFlag = false;
		Boolean phoneNumberFlag = false;

		if (message != null && message.hasText()) {
			String chat_id = message.getChatId().toString();

			String clientName = message.getFrom().getFirstName();
			if (message.getFrom().getLastName() != null) {
				clientName += " " + message.getFrom().getLastName();
			}

			if (spamMsgsFlags.containsKey(chat_id)) {
				spamMsgFlag = spamMsgsFlags.get(chat_id);
			}
			if (PhoneNumbersFlags.containsKey(chat_id)) {
				phoneNumberFlag = PhoneNumbersFlags.get(chat_id);
			}

			switch (message.getText()) {

			case "/sendSpam":
				sendSpam(message.getChatId().toString());
				break;
			case "/sendMsgToOneUser":
				sendSpamToOneUser(message);
				break;
			case "/start":
			case "🏡 Вернуться в главное меню 🏡":
				if (!members.containsKey(chat_id)) {
					members.put(chat_id, clientName);
					searchMemberInDb(chat_id);
				}
				spamMsgsFlags.remove(chat_id);
				PhoneNumbersFlags.remove(chat_id);

				keyboard = chooseKeyboard.startMenuKeyboard();
				sendPhoto(message, keyboard, "ntc_logo", "Вас приветствует Nissan Teana Club !");
				break;
			case "/rules":
				sendMsg(message,
						"Для получения возможности массовой рассылки Вам необходимо указать свой действующий телефонный номер"
								+ " в разделе Настройки и получить разрешение администратора. По всем вопросам обращайтесь к "
								+ "разработчику данного ресурса https://vk.com/zorindm");

				break;
			// настройки
			case "⚙️ Настройки ⚙️":
				keyboard = chooseKeyboard.SettingsKeyboard();
				sendMsg(message, keyboard, "Выберите интересующую Вас функцию.");
				break;
			case "☎️ Задать номер телефона ☎️":
				PhoneNumbersFlags.put(chat_id, true);
				keyboard = chooseKeyboard.PhoneNumberChekMsgKeyboard();
				String msgToStep1 = message.getFrom().getFirstName() + ", пожалуйста, отправьте нам номер телефона.";
				sendMsg(message, keyboard, msgToStep1);
				break;
			case "☎️ Отправить номер телефона ☎️":
				PhoneNumbersFlags.remove(chat_id);
				keyboard = chooseKeyboard.startMenuKeyboard();
				Database.setPhoneNumber(chat_id, PhoneNumbers.get(chat_id));
				sendMsg(message, keyboard, "Номер телефона добавлен !");
				break;
			case "❌ Отписаться от рассылки":
				Database.unsubscribeFromUsers(chat_id);
				sendMsg(message, "Вы отписались от рассылки других пользователей !");
				break;
			case "Подписаться на рассылку ✅":
				Database.subscribeFromUsers(chat_id);
				sendMsg(message, "Вы подписались на рассылку от других пользователей !");
				break;
			case "✏️ Массовая рассылка ✏️":

				String permission = Database.getPermissionForSpam(chat_id);
				if (permission.equals("off")) {
					keyboard = chooseKeyboard.startMenuKeyboard();
					sendMsg(message, keyboard,
							"Извините, Вам запрещена массовая рассылка. Воспользуйтесь командой /rules для ознакомления с условиями рассылки.");
					break;
				}

				spamMsgsFlags.put(chat_id, true);
				keyboard = chooseKeyboard.spamChekMsgKeyboard();
				String msgToStep3 = message.getFrom().getFirstName()
						+ ", пожалуйста, Введите сообщение. Максимальная длина 150 символов.";
				sendMsg(message, keyboard, msgToStep3);
				break;
			case "Отправить ✅":
				if (!spamMsgs.containsKey(chat_id) || spamMsgs.get(chat_id).equals("!#tooLong!#")) {
					return;
				}
				spamMsgsFlags.remove(chat_id);
				keyboard = chooseKeyboard.startMenuKeyboard();
				sendMsg(message, keyboard, "Ожидайте, идет отправка...");
				sendSpamFromUser(chat_id, spamMsgs.get(chat_id));
				String msgToStep2 = "Ваше сообщение отправлено !.";
				sendMsg(message, msgToStep2);
				spamMsgs.remove(chat_id);
				break;

			case "⛔️ Отключиться от бота ⛔️":
				Database.disconnectUserFromBot(chat_id);
				sendMsg(message, "Вы отключились от бота. Для возобновления просто просто продолжите общение !");
				break;
			// case "🏡 Вернуться в главное меню 🏡":
			// keyboard = chooseKeyboard.startMenuKeyboard();
			// sendPhoto(message, keyboard, "ntc_logo.jpg", "Вас приветствует Nissan Teana
			// Club !");
			// break;
			case "🚘 Часто задаваемые вопросы 🚘":
				keyboard = chooseKeyboard.faqKeyboard();
				sendMsg(message, keyboard, "Выберите интересующий Вас раздел.");
				break;
			case "⭐️ Полезные ссылки ⭐️":
				sendLinks(message);
				break;
			// общее меню
			case "🛠 Автосервисы 🛠":
				sendServices(message);
				break;
			case "❔ Общие вопросы ❔":
				sendFaqList(message, "faq_general");
				break;
			case "🚗 Teana J31":
				sendFaqList(message, "faq_j31");
				break;
			case "🚗 Teana J32":
				sendFaqList(message, "faq_j32");
				break;
			case "🚗 Teana L33":
				sendFaqList(message, "faq_l33");
				break;

			case "💧 Жидкости и масла 💧":
				sendFaqList(message, "faq_liquids");
				break;
			case "🚙 Новости Авто 🚙":
				sendMsg(message, "Загрузка...");
				sendAutoNews(message);
				break;
			default:
				if (!spamMsgFlag && !phoneNumberFlag) {
					sendMsg(message, "Неправильная команда");
					break;
				}
			}
		} else if (update.hasCallbackQuery() && !phoneNumberFlag && !spamMsgFlag) {
			String[] tmp = update.getCallbackQuery().getData().split("!@!");
			String call_data = tmp[0];
			String tableName = tmp[1];
			String chat_id = update.getCallbackQuery().getMessage().getChatId().toString();
			sendFaqAnswer(call_data, chat_id, tableName);
		}

		if (update.hasMessage() && spamMsgFlag) {
			if (update.getMessage().getText().length() > 150) {
				checkSpamMsgFromUser(message, keyboard, message.getChatId().toString(), "!#tooLong!#");
			} else {
				checkSpamMsgFromUser(message, keyboard, message.getChatId().toString(), update.getMessage().getText());
			}
		}
		if (update.hasMessage() && phoneNumberFlag) {
			checkPhoneNumber(message, keyboard, message.getChatId().toString(), update.getMessage().getText());
		}
	}

	private void searchMemberInDb(String chat_id) {

		if (Database.getMemberChatId(chat_id)) {
			return;
		}
		Database.addNewMember(chat_id, members.get(chat_id));
	}

	private void sendMsg(Message message, String text) {

		sendMessage.setChatId(message.getChatId().toString());
		sendMessage.setReplyToMessageId(message.getMessageId());
		sendMessage.setText(text);
		try {
			execute(sendMessage);
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
	}

	private void sendMsg(Message message, ArrayList<KeyboardRow> keyboard, String text) {

		sendMessage.enableMarkdown(true);

		ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		replyKeyboardMarkup.setSelective(true);
		replyKeyboardMarkup.setResizeKeyboard(true);
		replyKeyboardMarkup.setOneTimeKeyboard(false);
		replyKeyboardMarkup.setKeyboard(keyboard);

		sendMessage.setChatId(message.getChatId().toString());
		sendMessage.setReplyToMessageId(message.getMessageId());
		sendMessage.setText(text);
		try {
			execute(sendMessage);
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
	}

	private void sendPhoto(Message message, ArrayList<KeyboardRow> keyboard, String fileName, String text) {

		SendPhoto sendPhotoRequest = new SendPhoto();
		File file = new File("photos/" + fileName + ".jpg");
		ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();

		replyKeyboardMarkup.setSelective(true);
		replyKeyboardMarkup.setResizeKeyboard(true);
		replyKeyboardMarkup.setOneTimeKeyboard(false);
		replyKeyboardMarkup.setKeyboard(keyboard);

		sendPhotoRequest.setChatId(message.getChatId().toString());
		sendPhotoRequest.setNewPhoto(file);
		sendPhotoRequest.setCaption(text);
		sendPhotoRequest.setReplyMarkup(replyKeyboardMarkup);

		try {
			sendPhoto(sendPhotoRequest);
		} catch (TelegramApiException ex) {
			file = new File("photos/missing_foto.jpg");
			sendPhotoRequest.setNewPhoto(file);
			try {
				sendPhoto(sendPhotoRequest);
			} catch (TelegramApiException e) {
				e.printStackTrace();
			}
		}
	}

	private void sendFaqList(Message message, String tableName) {
		try {
			ResultSet rs = Database.getFAQ(tableName);

			while (rs.next()) {
				SendPhoto sendPhotoRequest = new SendPhoto();
				File file;
				String photoName = rs.getString(3) + ".jpg";

				InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
				List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
				List<InlineKeyboardButton> rowInline = new ArrayList<>();
				if (photoName == null || photoName.equals("")) {
					file = new File("photos/missing_foto.jpg");
				} else {
					file = new File("photos/faq/" + photoName);
				}

				rowInline.add(new InlineKeyboardButton().setText("Решение проблемы")
						.setCallbackData(rs.getString(1) + "!@!" + tableName));
				rowsInline.add(rowInline);
				markupInline.setKeyboard(rowsInline);

				sendPhotoRequest.setChatId(message.getChatId().toString());
				sendPhotoRequest.setNewPhoto(file);
				sendPhotoRequest.setCaption(rs.getString(2));
				sendPhotoRequest.setReplyMarkup(markupInline);

				try {
					sendPhoto(sendPhotoRequest);
				} catch (TelegramApiException ex) {
					file = new File("photos/missing_foto.jpg");
					sendPhotoRequest.setNewPhoto(file);
					sendPhoto(sendPhotoRequest);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendFaqAnswer(String call_data, String chat_id, String tableName) {
		String msg = "";
		String photoName = "";

		try {
			ResultSet rs = Database.getDecision(call_data, tableName);
			msg = "Проблема : " + rs.getString(3) + "\nРешение: " + rs.getString(1);
			photoName = rs.getString(2) + ".jpg";

			SendPhoto sendPhotoRequest = new SendPhoto();
			File file;

			if (photoName == null || photoName.equals("")) {
				file = new File("photos/missing_foto.jpg");
			} else {
				file = new File("photos/faq/" + photoName);
			}

			sendPhotoRequest.setChatId(chat_id);
			sendPhotoRequest.setNewPhoto(file);
			sendPhotoRequest.setCaption(msg);

			try {
				sendPhoto(sendPhotoRequest);
			} catch (TelegramApiException ex) {
				file = new File("photos/missing_foto.jpg");
				sendPhotoRequest.setNewPhoto(file);
				sendPhoto(sendPhotoRequest);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendLinks(Message message) {
		try {
			ResultSet rs = Database.getLinks();

			while (rs.next()) {
				SendPhoto sendPhotoRequest = new SendPhoto();
				File file;
				String photoName = rs.getString(3) + ".jpg";

				InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
				List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
				List<InlineKeyboardButton> rowInline = new ArrayList<>();
				if (photoName == null || photoName.equals("")) {
					file = new File("photos/missing_foto.jpg");
				} else {
					file = new File("photos/links/" + photoName);
				}

				rowInline.add(new InlineKeyboardButton().setText("Перейти").setUrl(rs.getString(4)));
				rowsInline.add(rowInline);
				markupInline.setKeyboard(rowsInline);

				sendPhotoRequest.setChatId(message.getChatId().toString());
				sendPhotoRequest.setNewPhoto(file);
				sendPhotoRequest.setCaption(rs.getString(2));
				sendPhotoRequest.setReplyMarkup(markupInline);

				try {
					sendPhoto(sendPhotoRequest);
				} catch (TelegramApiException ex) {
					file = new File("photos/missing_foto.jpg");
					sendPhotoRequest.setNewPhoto(file);
					sendPhoto(sendPhotoRequest);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendServices(Message message) {
		String msg = "";
		String chat_id = message.getChatId().toString();
		try {
			ResultSet rs = Database.getServices();

			while (rs.next()) {
				SendPhoto sendPhotoRequest = new SendPhoto();
				File file;
				String photoName = rs.getString(3) + ".jpg";
				msg = rs.getString(1) + "\n" + rs.getString(2);

				if (photoName == null || photoName.equals("")) {
					file = new File("photos/missing_foto.jpg");
				} else {
					file = new File("photos/services/" + photoName);
				}

				sendPhotoRequest.setChatId(chat_id);
				sendPhotoRequest.setNewPhoto(file);
				sendPhotoRequest.setCaption(msg);

				try {
					sendPhoto(sendPhotoRequest);
				} catch (TelegramApiException ex) {
					file = new File("photos/missing_foto.jpg");
					sendPhotoRequest.setNewPhoto(file);
					sendPhoto(sendPhotoRequest);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendSpam(String chat_id) {

		if (!chat_id.equals(adminChatId)) {
			return;
		}

		ResultSet rs = Database.getMembers();
		SendPhoto sendPhotoRequest = new SendPhoto();
		File file = new File("photos/" + spamPhotoName + ".jpg");

		sendPhotoRequest.setNewPhoto(file);
		try {
			while (rs.next()) {
				sendPhotoRequest.setChatId(rs.getString(1));
				sendPhotoRequest.setCaption("Уважаемый " + rs.getString(2) + "!\n" + spamDescription);
				try {
					sendPhoto(sendPhotoRequest);
				} catch (TelegramApiException ex) {
					ex.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendSpamToOneUser(Message message) {
		String chat_id = message.getChatId().toString();
		if (!chat_id.equals(adminChatId)) {
			return;
		}
		String name = Database.getOneMemberName(chatIdForMsgToOneUser);
		String caption = "Уважаемый " + name + "! " + spamForOneUser;
		SendPhoto sendPhotoRequest = new SendPhoto();
		File file = new File("photos/ntc_telegram.jpg");

		sendPhotoRequest.setNewPhoto(file);
		sendPhotoRequest.setCaption(caption);
		sendPhotoRequest.setChatId(chatIdForMsgToOneUser);
		try {
			sendPhoto(sendPhotoRequest);
		} catch (TelegramApiException ex) {
			ex.printStackTrace();
		}

		sendMessage.setChatId(message.getChatId().toString());
		sendMessage.setReplyToMessageId(message.getMessageId());
		sendMessage.setText("Сообщение пользователю " + name + " отправлено.");
		try {
			execute(sendMessage);
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
	}

	private void checkSpamMsgFromUser(Message message, ArrayList<KeyboardRow> keyboard, String chat_id, String msg) {

		ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		replyKeyboardMarkup.setSelective(true);
		replyKeyboardMarkup.setResizeKeyboard(true);
		replyKeyboardMarkup.setOneTimeKeyboard(false);
		replyKeyboardMarkup.setKeyboard(keyboard);

		sendMessage.setChatId(chat_id);
		sendMessage.setReplyToMessageId(message.getMessageId());
		String nextStep;

		if (msg.equals("!#tooLong!#")) {
			nextStep = "Извините, сообщение превышает максимальный размер. Пожалуйста, укоротите Ваше сообщение.";
		} else {
			nextStep = "Нажмите кнопку \"Отправить ✅\" если сообщение верно, либо отправьте нам другое сообщение.";
		}

		if (!msg.equals("🏡 Вернуться в главное меню 🏡") && !msg.equals("Отправить ✅")) {
			spamMsgs.put(chat_id, msg);
		} else {
			return;
		}
		sendMessage.setText(nextStep);
		try {
			execute(sendMessage);
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
	}

	private void checkPhoneNumber(Message message, ArrayList<KeyboardRow> keyboard, String chat_id, String msg) {

		ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		replyKeyboardMarkup.setSelective(true);
		replyKeyboardMarkup.setResizeKeyboard(true);
		replyKeyboardMarkup.setOneTimeKeyboard(false);
		replyKeyboardMarkup.setKeyboard(keyboard);

		sendMessage.setChatId(chat_id);
		sendMessage.setReplyToMessageId(message.getMessageId());

		String nextStep = "Нажмите кнопку \"☎️ Отправить номер телефона ☎️\" если номер верный, либо отправьте нам другой номер.";
		if (!msg.equals("🏡 Вернуться в главное меню 🏡") && !msg.equals("☎️ Отправить номер телефона ☎️")) {
			PhoneNumbers.put(chat_id, msg);
		} else {
			return;
		}

		sendMessage.setText(nextStep);
		try {
			execute(sendMessage);
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
	}

	private void sendSpamFromUser(String chat_id, String msg) {

		ResultSet rs = Database.getMembers();
		ResultSet rs2 = Database.getSpamer(chat_id);

		String spamerName = "";
		String spamerPhoneNumber = "";
		try {
			spamerName = rs2.getString(1);
			spamerPhoneNumber = rs2.getString(2);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		SendPhoto sendPhotoRequest = new SendPhoto();
		File file = new File("photos/SpamFromUser.jpg");

		sendPhotoRequest.setNewPhoto(file);
		try {
			while (rs.next()) {
				if (rs.getString(3).equals("on")) {
					sendPhotoRequest.setChatId(rs.getString(1));
					sendPhotoRequest
							.setCaption("Сообщение от " + spamerName + "\nТелефон: " + spamerPhoneNumber + "\n" + msg);
					try {
						sendPhoto(sendPhotoRequest);
					} catch (TelegramApiException ex) {
						ex.printStackTrace();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendAutoNews(Message message) {

		List<News> list = new ArrayList<>();

		String title = "";
		Document doc;
		Elements elms;
		String link = "https://auto.mail.ru/";

		try {
			doc = Jsoup.connect(link + "/news/").get();
			title = doc.title();
			elms = doc.getElementsByAttributeValue("class", "newsitem__title link-holder");

			elms.forEach(elm -> {
				Element tmp = elm.child(0);
				String url = link + elm.attr("href");
				String newsText = elm.child(0).text();

				list.add(new News(newsText, url));
			});

		} catch (IOException e) {
			e.printStackTrace();
		}

		sendMessage.setChatId(message.getChatId().toString());
		sendMessage.setReplyToMessageId(message.getMessageId());
		sendMessage.setText(title);
		try {
			execute(sendMessage);
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}

		list.forEach(news -> {
			InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
			List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
			List<InlineKeyboardButton> rowInline = new ArrayList<>();

			rowInline.add(new InlineKeyboardButton().setText("Читать новость").setUrl(news.getUrl()));
			rowsInline.add(rowInline);
			markupInline.setKeyboard(rowsInline);
			sendMessage.setChatId(message.getChatId().toString());
			sendMessage.setText(news.getName());
			sendMessage.setReplyMarkup(markupInline);
			try {
				execute(sendMessage);
			} catch (TelegramApiException e) {
				e.printStackTrace();
			}
		});

	}

	public static void setSpamSettings(String spamPhoto, String description) {
		spamPhotoName = spamPhoto;
		spamDescription = description;
	}

	public static void setSpamForOneUserSettings(String chatId, String description) {
		chatIdForMsgToOneUser = chatId;
		spamForOneUser = description;
	}
}

class News {
	private String name;
	private String url;

	public News(String name, String url) {
		this.name = name;
		this.url = url;

	}

	public String getName() {
		return name;
	}

	public String getUrl() {
		return url;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
