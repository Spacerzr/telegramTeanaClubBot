package ru.dgzorin.teanaclubbot.telegrambot;

import java.util.ArrayList;

import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;

public class Keyboards {
	private KeyboardButton kbFaq = new KeyboardButton("🚘 Часто задаваемые вопросы 🚘");
	private KeyboardButton kbLinks = new KeyboardButton("⭐️ Полезные ссылки ⭐️");
	private KeyboardButton kbServices = new KeyboardButton("🛠 Автосервисы 🛠");
	private KeyboardButton kbHome = new KeyboardButton("🏡 Вернуться в главное меню 🏡");

	private KeyboardButton kbSettings = new KeyboardButton("⚙️ Настройки ⚙️");
	private KeyboardButton kbAddPhone = new KeyboardButton("☎️ Задать номер телефона ☎️");
	private KeyboardButton kbStopSpam = new KeyboardButton("❌ Отписаться от рассылки");
	private KeyboardButton kbStartSpam = new KeyboardButton("Подписаться на рассылку ✅");
	private KeyboardButton kbUserSpam = new KeyboardButton("✏️ Массовая рассылка ✏️");
	private KeyboardButton kbDisconnectFromBot = new KeyboardButton("⛔️ Отключиться от бота ⛔️");
	private KeyboardButton kbSendSpamFromOneUser = new KeyboardButton("Отправить ✅");
	private KeyboardButton kbSetPhonenumber = new KeyboardButton("☎️ Отправить номер телефона ☎️");

	private KeyboardButton kbTeanaJ31 = new KeyboardButton("🚗 Teana J31");
	private KeyboardButton kbTeanaJ32 = new KeyboardButton("🚗 Teana J32");
	private KeyboardButton kbTeanaL33 = new KeyboardButton("🚗 Teana L33");

	private KeyboardButton kbFaqJ32 = new KeyboardButton("✴️ Общие вопросы J32 ✴️");
	private KeyboardButton kbLiquids = new KeyboardButton("💧 Жидкости и масла 💧");
	private KeyboardButton kbFaqGeneral = new KeyboardButton("❔ Общие вопросы ❔");

	private KeyboardButton kbGetAutoNews = new KeyboardButton("🚙 Новости Авто 🚙");

	ArrayList<KeyboardRow> startMenuKeyboard() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();

		keyboardFirstRow.add(kbFaq);

		KeyboardRow keyboardSecondRow = new KeyboardRow();
		keyboardSecondRow.add(kbLinks);
		keyboardSecondRow.add(kbServices);

		KeyboardRow keyboardThirdRow = new KeyboardRow();
		keyboardThirdRow.add(kbSettings);
		keyboardThirdRow.add(kbUserSpam);

		KeyboardRow keyboardFourthRow = new KeyboardRow();
		keyboardFourthRow.add(kbGetAutoNews);

		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);
		keyboard.add(keyboardThirdRow);
		keyboard.add(keyboardFourthRow);

		return keyboard;
	}

	ArrayList<KeyboardRow> faqKeyboard() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();

		keyboardFirstRow.add(kbFaqGeneral);
		keyboardFirstRow.add(kbLiquids);

		KeyboardRow keyboardSecondRow = new KeyboardRow();
		keyboardSecondRow.add(kbTeanaJ31);
		keyboardSecondRow.add(kbTeanaJ32);
		keyboardSecondRow.add(kbTeanaL33);

		KeyboardRow keyboardThirdRow = new KeyboardRow();
		keyboardThirdRow.add(kbHome);

		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);
		keyboard.add(keyboardThirdRow);

		return keyboard;
	}

	ArrayList<KeyboardRow> SettingsKeyboard() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();

		keyboardFirstRow.add(kbAddPhone);

		KeyboardRow keyboardSecondRow = new KeyboardRow();
		keyboardSecondRow.add(kbStopSpam);
		keyboardSecondRow.add(kbStartSpam);

		KeyboardRow keyboardThirdRow = new KeyboardRow();
		keyboardThirdRow.add(kbDisconnectFromBot);
		keyboardThirdRow.add(kbHome);

		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);
		keyboard.add(keyboardThirdRow);

		return keyboard;
	}

	ArrayList<KeyboardRow> spamChekMsgKeyboard() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();

		keyboardFirstRow.add(kbSendSpamFromOneUser);

		KeyboardRow keyboardSecondRow = new KeyboardRow();
		keyboardSecondRow.add(kbHome);

		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);

		return keyboard;
	}

	ArrayList<KeyboardRow> PhoneNumberChekMsgKeyboard() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();

		keyboardFirstRow.add(kbSetPhonenumber);

		KeyboardRow keyboardSecondRow = new KeyboardRow();
		keyboardSecondRow.add(kbHome);

		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);

		return keyboard;
	}
}
