package ru.dgzorin.teanaclubbot.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Database {
	private static Connection connection;

	private static String dbName = "teanaclubdb";

	private static PreparedStatement ps;

	public synchronized static void connect() throws Exception {

		Class.forName("org.sqlite.JDBC");
		connection = DriverManager.getConnection("JDBC:sqlite:" + dbName + ".db");

	}

	public synchronized static void disconnect() {
		try {
			connection.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	// методы для получения данных

	public static ResultSet getData(String tableName) {

		try {
			ps = connection.prepareStatement(
					"SELECT id, name, description, photoname, decision, photonamedecision FROM " + tableName + ";");
			ResultSet rs = ps.executeQuery();
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static ResultSet getOneStringData(String tableName, String description) {

		try {
			ps = connection
					.prepareStatement("SELECT name, description, photoname, decision, photonamedecision, id FROM "
							+ tableName + " WHERE description = '" + description + "';");
			ResultSet rs = ps.executeQuery();

			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static ResultSet getLinksData() {

		try {
			ps = connection.prepareStatement("SELECT id, name, description, photoname, url FROM links;");
			ResultSet rs = ps.executeQuery();
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static ResultSet getOneStringLinksData(String description) {

		try {
			ps = connection.prepareStatement("SELECT name, description, photoname, url, id FROM links"
					+ " WHERE description = '" + description + "';");
			ResultSet rs = ps.executeQuery();

			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static ResultSet getServicesData() {

		try {
			ps = connection.prepareStatement("SELECT id, name, description, photoname FROM services;");
			ResultSet rs = ps.executeQuery();
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static ResultSet getOneStringServicesData(String name) {

		try {
			ps = connection.prepareStatement(
					"SELECT name, description, photoname, id FROM services" + " WHERE name = '" + name + "';");
			ResultSet rs = ps.executeQuery();

			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	// public static ResultSet getId(String tableName, Integer id) {
	//
	// try {
	// ps = connection.prepareStatement("SELECT name, description, photoname,
	// decision, photonamedecision FROM "
	// + tableName + " WHERE id = " + id + ";");
	// ResultSet rs = ps.executeQuery();
	//
	// return rs;
	// } catch (SQLException e) {
	// e.printStackTrace();
	// return null;
	// }
	// }
	public static ResultSet getFAQ(String tableName) {

		try {
			ps = connection.prepareStatement("SELECT name, description, photoname FROM " + tableName + ";");
			ResultSet rs = ps.executeQuery();
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static ResultSet getDecision(String name, String tableName) {
		try {
			ps = connection.prepareStatement("SELECT decision, photonamedecision, description FROM " + tableName
					+ " WHERE name = '" + name + "';");
			ResultSet rs = ps.executeQuery();
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static ResultSet getLinks() {

		try {
			ps = connection.prepareStatement("SELECT name, description, photoname, url FROM links;");
			ResultSet rs = ps.executeQuery();
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static ResultSet getServices() {

		try {
			ps = connection.prepareStatement("SELECT name, description, photoname FROM services;");
			ResultSet rs = ps.executeQuery();
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static boolean getMemberChatId(String chat_id) {
		try {
			ps = connection.prepareStatement("SELECT chat_id FROM members;");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				if (rs.getString(1).equals(chat_id)) {
					return true;
				}
			}
			return false;

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public static ResultSet getMembers() {
		try {
			ps = connection.prepareStatement("SELECT chat_id, name, spam, phoneNumber, spam_permission FROM members;");
			ResultSet rs = ps.executeQuery();
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static ResultSet getOneMember(String name) {

		try {
			ps = connection.prepareStatement(
					"SELECT chat_id, name, spam, phoneNumber, spam_permission FROM members WHERE name = '" + name
							+ "';");
			ResultSet rs = ps.executeQuery();

			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String getOneChatId(String name) {
		try {
			ps = connection.prepareStatement("SELECT chat_id FROM members WHERE name ='" + name + "';");
			ResultSet rs = ps.executeQuery();
			return rs.getString(1);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String getOneMemberName(String chat_id) {
		try {
			ps = connection.prepareStatement("SELECT name FROM members WHERE chat_id = '" + chat_id + "';");
			ResultSet rs = ps.executeQuery();
			return rs.getString(1);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void addNewMember(String chat_id, String name) {
		try {
			ps = connection.prepareStatement("INSERT INTO members (chat_id, name, spam, spam_permission) VALUES ('"
					+ chat_id + "', '" + name + "', 'on', 'off');");
			// psAddNewMember = connection
			// .prepareStatement("INSERT INTO members (chat_id, name) VALUES (?,?);");
			// psAddNewMember.setString(1, chat_id);
			// psAddNewMember.setString(2, name);

			ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void setPhoneNumber(String chat_id, String phoneNumber) {
		try {
			ps = connection.prepareStatement(
					"UPDATE members SET phoneNumber ='" + phoneNumber + "' WHERE chat_id = '" + chat_id + "';");
			// psAddNewMember = connection
			// .prepareStatement("INSERT INTO members (chat_id, name) VALUES (?,?);");
			// psAddNewMember.setString(1, chat_id);
			// psAddNewMember.setString(2, name);

			ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static ResultSet getSpamer(String chat_id) {
		try {
			ps = connection
					.prepareStatement("SELECT name, phoneNumber FROM members WHERE chat_id = '" + chat_id + "';");

			ResultSet rs = ps.executeQuery();

			return rs;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String getPermissionForSpam(String chat_id) {
		try {
			ps = connection.prepareStatement("SELECT spam_permission FROM members WHERE chat_id = '" + chat_id + "';");

			ResultSet rs = ps.executeQuery();

			return rs.getString(1);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void unsubscribeFromUsers(String chat_id) {
		try {
			ps = connection.prepareStatement("UPDATE members SET spam = 'off' WHERE chat_id = '" + chat_id + "';");
			ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	// методы для редактирования данных

	public static void addNewToFaq(String tableName, Integer id, String description, String photoname, String decision,
			String photonamedecision) {
		try {
			ps = connection.prepareStatement("INSERT INTO " + tableName
					+ " (id, name, description, photoname, decision, photonamedecision) VALUES (" + id + ", \""
					+ photoname + "\", \"" + description + "\", \"" + photoname + "\", \"" + decision + "\", \""
					+ photonamedecision + "\");");
			ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void editDataFaq(String tableName, Integer id, String description, String photoname, String decision,
			String photonamedecision) {
		try {
			ps = connection.prepareStatement("UPDATE " + tableName + " SET name = '" + photoname + "', description = '"
					+ description + "', photoname = '" + photoname + "', decision = '" + decision
					+ "', photonamedecision = '" + photonamedecision + "' WHERE id = " + id + ";");
			ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void removeFromData(String tableName, String description) {
		try {
			ps = connection
					.prepareStatement("DELETE FROM " + tableName + " WHERE description = \"" + description + "\";");
			ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void addNewToLinks(Integer id, String description, String photoname, String url) {
		try {
			ps = connection.prepareStatement("INSERT INTO links (id, name, description, photoname, url) VALUES (" + id
					+ ", \"" + photoname + "\", \"" + description + "\", \"" + photoname + "\", \"" + url + "\");");
			ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void editDataLinks(Integer id, String description, String photoname, String url) {
		try {
			ps = connection.prepareStatement("UPDATE links SET name = '" + photoname + "', description = '"
					+ description + "', photoname = '" + photoname + "', url = '" + url + "' WHERE id = " + id + ";");
			ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void addNewToServices(Integer id, String name, String description, String photoname) {
		try {
			ps = connection.prepareStatement("INSERT INTO services (id, name, description, photoname) VALUES (" + id
					+ ", \"" + name + "\", \"" + description + "\", \"" + photoname + "\");");
			ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void editDataServices(Integer id, String name, String description, String photoname) {
		try {
			ps = connection.prepareStatement("UPDATE services SET name = '" + name + "', description = '" + description
					+ "', photoname = '" + photoname + "' WHERE id = " + id + ";");
			ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void removeFromServices(String name) {
		try {
			ps = connection.prepareStatement("DELETE FROM services WHERE name = \"" + name + "\";");
			ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void editMember(String name, String phoneNumber, String spam_permission) {
		try {
			ps = connection.prepareStatement("UPDATE members SET phoneNumber = '" + phoneNumber
					+ "', spam_permission = '" + spam_permission + "' WHERE name = '" + name + "';");
			ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// методы подписки/отписки
	public static void subscribeFromUsers(String chat_id) {
		try {
			ps = connection.prepareStatement("UPDATE members SET spam = 'on' WHERE chat_id = '" + chat_id + "';");
			ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void disconnectUserFromBot(String chat_id) {
		try {
			ps = connection.prepareStatement("DELETE FROM members WHERE chat_id = '" + chat_id + "';");
			ps.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
